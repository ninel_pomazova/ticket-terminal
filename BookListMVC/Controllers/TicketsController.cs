﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookListMVC.Models;
using Tickets.Models;
using Tickets.BusinessLogic.Abstracrions;

namespace Tickets.Controllers
{
    public class TicketsController : Controller
    {
       public Performance Performance { get; set; }
        [BindProperty]
        public  Ticket Ticket { get; set; }
        private readonly ApplicationDbContext _context;
        private readonly ITicketService _service;

        public TicketsController(ApplicationDbContext context, ITicketService service)
        {
            _context = context;
            _service = service;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Tickets.Include(t => t.PPerformanceName).Include(t => t.PPerformanceName).Include(t => t.PPerformanceName).Include(t => t.PPerformanceName);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets.FirstOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // GET: Tickets/Create
        public async Task<IActionResult> Create(int? id)
        {
            var perf = await _context.Performances.FindAsync(id);
            

            if ( perf.Id != id)
                {
                    return NotFound();
                }


            Ticket t = new Ticket
            {

                PerfId = perf.Id,
                PerfName = perf.Name,
                PerfPrice = perf.Price,
                PerfDate = perf.Date
            };
            

            return View(t);
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PersonName,Email,Phone,CreditCard,PerfId,PerfName,PerfDate,PerfPrice,IsConfirmed")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ticket);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PersonName,Email,Phone,CreditCard,PerfId,PerfName,PerfDate,PerfPrice,IsConfirmed")] Ticket ticket)
        {

            if (id != ticket.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.UpdateAsync(ticket);
                   // await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TicketExists(ticket.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets
                //.Include(t => t.PPerformanceDate)//.ThenInclude(u => u.TPerfDates)
               // .Include(t => t.PPerformanceId)//.ThenInclude(u => u.TPerfIds)
                .Include(t => t.PPerformanceName)//.ThenInclude(u => u.TPerfNames)
                //.Include(t => t.PPerformancePrice)//.ThenInclude(u => u.TPerfPrices)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {



            var result = await _service.DeleteAsync(id);
           /* return Ok();// insertedItem;

            var ticket = await _context.Tickets.FindAsync(id);
            if(ticket == null)
            {
                return null;
            }
            _context.Tickets.Remove(ticket);
            await _context.SaveChangesAsync();*/
            return RedirectToAction(nameof(Index));
        }

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.Id == id);
        }
    }
}

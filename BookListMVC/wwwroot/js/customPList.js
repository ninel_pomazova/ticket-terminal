﻿
var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load1').DataTable({
        "ajax": {
            "url": "/PerfList/getall/",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "name", "width": "15%" },
            { "data": "venue", "width": "15%" },
            { "data": "date", "width": "15%" },
            { "data": "seatsCount", "width": "15%" },
            { "data": "price", "width": "15%" },
            { "data": "info", "width": "15%" },
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                       <p> <a href="/Tickets/Create?id=${data}" class='btn btn-success text-white' style='cursor:pointer; width:70px;'>
                            Buy
                        </a></p>
                        <p> <a href="/PerfList/Details?id=${data}" class="btn btn-primary">
                        Details
                        </a></p>
                        </div>`;
                }, "width": "40%"
            }
        ],
        "language": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    });
}

function Delete(url) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}


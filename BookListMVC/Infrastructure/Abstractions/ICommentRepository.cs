﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.Infrastructure.Abstractions
{
    public interface ICommentRepository : IRepository<Comment, int>
    {
    }
}

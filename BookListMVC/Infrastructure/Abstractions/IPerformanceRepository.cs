﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.Infrastructure.Abstractions
{
    public interface IPerformanceRepository : IRepository<Performance, int>
    {
    }
}

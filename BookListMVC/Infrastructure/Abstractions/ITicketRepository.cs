﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.Infrastructure.Abstractions
{
   public interface ITicketRepository : IRepository<Ticket, int>
    {
        Task<Ticket> GetById(int userId);
    }
}

﻿using BookListMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace Tickets.Infrastructure.Repositories
{
    public class PerformanceRepository : BaseRepository<Performance, int>, IPerformanceRepository
    {
        public override IUnitOfWork UnitOfWork { get; }


        public PerformanceRepository(ApplicationDbContext context) : base(context)
        {
            UnitOfWork = context;
        }
    }
}

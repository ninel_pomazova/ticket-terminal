﻿using BookListMVC.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;
using System;
using System.Collections.Generic;


namespace Tickets.Infrastructure.Repositories
{
    public class TicketsRepository : BaseRepository<Ticket, int>, ITicketRepository
    {
    
        public override IUnitOfWork UnitOfWork { get; }


        public TicketsRepository(ApplicationDbContext context) : base(context)
        {
            UnitOfWork = context;
        }



        public async Task<Ticket> GetById(int ticketId)
        {
            return await _dbSet.FirstOrDefaultAsync(u => u.Id == ticketId);
        }

    }
}

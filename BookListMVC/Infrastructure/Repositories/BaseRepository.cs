﻿
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Infrastructure.Abstractions;


namespace Tickets.Infrastructure.Repositories
{
    public abstract class BaseRepository<T, TKey> : IRepository<T, TKey> where T : class, Tickets.Models.Abstractions.IEntity<TKey>
    {
        protected readonly DbContext _context;
        protected readonly DbSet<T> _dbSet;

        public abstract IUnitOfWork UnitOfWork { get; }

        public BaseRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        virtual public Task<T> GetByIdAsync(TKey id)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public T Create(T item)
        {
            return _dbSet.Add(item).Entity;
        }

        public void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(T item)
        {
            _dbSet.Remove(item);
        }

        public async Task<IEnumerable<T>> GetAsync(Ardalis.Specification.ISpecification<T> specification)
        {
            return await ApplySpecification(specification).ToListAsync();
        }

        public async Task<T> GetSingleAsync(Ardalis.Specification.ISpecification<T> specification)
        {
            return await ApplySpecification(specification).FirstOrDefaultAsync();
        }

        public IQueryable<T> ApplySpecification(ISpecification<T> specification)
        {
            var evaluator = new SpecificationEvaluator<T>();
            return evaluator.GetQuery(_dbSet, specification);
        }


    }
}

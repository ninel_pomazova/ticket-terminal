﻿using BookListMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace Tickets.Infrastructure.Repositories
{
    public class CommentRepository : BaseRepository<Comment, int>, ICommentRepository
    {
        public override IUnitOfWork UnitOfWork { get; }


        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            UnitOfWork = context;
        }
    }
}

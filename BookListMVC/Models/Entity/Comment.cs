﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models.Abstractions;

namespace Tickets.Models
{
    public class Comment : IEntity<int>
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string PersonName { get; set; }

        [StringLength(300)]
        public string Text { get; set; }
    }
}

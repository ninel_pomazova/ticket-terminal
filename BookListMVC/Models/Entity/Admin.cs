﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models.Abstractions;

namespace Tickets.Models
{
    public class Admin : IEntity<int>
    {
        [Key]
        public int Id { get; set; }

        [StringLength(10)]
        [Required]
        public string Login { get; set; }

        [Required]
        [MaxLength(10)]
        public string Password { get; set; }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models.Abstractions;

namespace Tickets.Models
{
    public class Performance : IEntity<int>
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Venue { get; set; }

        [DataType("datetime()")]
        public DateTime Date { get; set; }

        [DefaultValue(100)]
        public int SeatsCount { get; set; }

        [DataType("money")]
        public decimal Price { get; set; }

        [StringLength(300)]
        public string Info { get; set; }

        public ICollection<Ticket> TPerfNames { get; set; }
        public ICollection<Ticket> TPerfIds { get; set; }
        public ICollection<Ticket> TPerfPrices { get; set; }
        public ICollection<Ticket> TPerfDates { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models.Abstractions;

namespace Tickets.Models
{
    public class Ticket : IEntity<int>
    {
        [Key]
        public int Id { get; set; }


        [StringLength(50)]
        public string PersonName { get; set; }

        [StringLength(50)]
        [Required]
        public string Email { get; set; }  
        
        [StringLength(13)]
        [Required]
        public string Phone { get; set; }    

        [StringLength(16)]
        [Required]
        public string CreditCard { get; set; }
        
        public int PerfId { get; set; }
        [StringLength(50)]

        public string PerfName { get; set; }

        [DataType("datetime()")]
        public DateTime PerfDate { get; set; }

        [DataType("money")]
        public decimal PerfPrice { get; set; }

        [DefaultValue(false)]
        public bool IsConfirmed { get; set; }



        public  Performance  PPerformanceName { get; set; }
        public  Performance  PPerformanceId { get; set; }
        public  Performance  PPerformancePrice { get; set; }
        public  Performance  PPerformanceDate { get; set; }

    }
}

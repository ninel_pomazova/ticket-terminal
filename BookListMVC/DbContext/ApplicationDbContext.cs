﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace BookListMVC.Models
{
    public class ApplicationDbContext : DbContext, IUnitOfWork
    {
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Performance> Performances { get; set; } 
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Admin> Admins { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ticket>()
                 .HasOne(p => p.PPerformanceId)
                 .WithMany(t => t.TPerfIds)
                 .HasForeignKey(p => p.PerfId)
                 .HasPrincipalKey(t => t.Id)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Ticket>()
               .HasOne(p => p.PPerformanceName)
               .WithMany(t => t.TPerfNames)
               .HasForeignKey(p => p.PerfName)
               .HasPrincipalKey(t => t.Name)
             .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Ticket>()
               .HasOne(p => p.PPerformancePrice)
               .WithMany(t => t.TPerfPrices)
               .HasForeignKey(p => p.PerfPrice)
               .HasPrincipalKey(t => t.Price)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Ticket>()
               .HasOne(p => p.PPerformanceDate)
               .WithMany(t => t.TPerfDates)
               .HasForeignKey(p => p.PerfDate)
               .HasPrincipalKey(t => t.Date)
               .OnDelete(DeleteBehavior.NoAction);
        }


    }
}

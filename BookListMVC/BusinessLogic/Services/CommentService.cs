﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Abstracrions;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace Tickets.BusinessLogic.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public async  Task<Comment> CreateAsync(Comment item)
        {
            var createdIem = _commentRepository.Create(item);

            await _commentRepository.UnitOfWork.SaveChangesAsync();

            return createdIem;
        }

        public async Task<Comment> DeleteAsync(int commentId)
        {
            var comment = await _commentRepository.GetByIdAsync(commentId);

            if (comment == null)
            {
                return null;
            }

            _commentRepository.Delete(comment);
            await _commentRepository.UnitOfWork.SaveChangesAsync();
            var res = await _commentRepository.GetByIdAsync(comment.Id);
            return comment;
        }

        public async Task<Comment> UpdateAsync(Comment item)
        {
            var comment = await _commentRepository.GetByIdAsync(item.Id);

            if (comment == null)
            {
                return null;
            }

            comment.Id = item.Id;
            comment.PersonName = item.PersonName;
            comment.Text = item.Text;

            _commentRepository.Update(comment);
            await _commentRepository.UnitOfWork.SaveChangesAsync();

            return comment;
        }
    }
}

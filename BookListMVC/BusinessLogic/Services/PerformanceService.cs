﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Abstracrions;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace Tickets.BusinessLogic.Services
{
    public class PerformanceService : IPerformanceService
    {
        private readonly IPerformanceRepository _performanceRepository;

        public PerformanceService(IPerformanceRepository performanceRepository)
        {
            _performanceRepository = performanceRepository;
        }

        public async Task<Performance> CreateAsync(Performance item)
        {

            var createdIem = _performanceRepository.Create(item);

            await _performanceRepository.UnitOfWork.SaveChangesAsync();
            
            return createdIem;
        }

        public async Task<Performance> DeleteAsync(int perfId)
        {
            var perf = await _performanceRepository.GetByIdAsync(perfId);


            if (perf == null)
            {
                return null;
            }
            

            _performanceRepository.Delete(perf);
            await _performanceRepository.UnitOfWork.SaveChangesAsync();


            return perf;
        }

        public async Task<Performance> UpdateAsync(Performance item)
        {
            var perf = await _performanceRepository.GetByIdAsync(item.Id);
            if (perf == null)
            {
                return null;
            }

            perf.Id = item.Id;
            perf.Price = item.Price;
            perf.Info = item.Info;
            perf.Name = item.Name;
            perf.SeatsCount = item.SeatsCount;
            perf.TPerfDates = item.TPerfDates;
            perf.TPerfIds = item.TPerfIds;
            perf.TPerfNames = item.TPerfNames;
            perf.TPerfPrices = item.TPerfPrices;
            perf.Date = item.Date;
            perf.Venue = item.Venue;

            _performanceRepository.Update(perf);
            await _performanceRepository.UnitOfWork.SaveChangesAsync();

            return perf;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Abstracrions;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;

namespace Tickets.BusinessLogic.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly IPerformanceRepository _performanceRepository;


        public TicketService(ITicketRepository ticketRepository, IPerformanceRepository performanceRepository)
        {
            _ticketRepository = ticketRepository;
            _performanceRepository = performanceRepository;
        }

        public async Task<Ticket> CreateAsync(Ticket item)
        {
            var existedItem = await _performanceRepository.GetByIdAsync(item.PerfId);
            if (existedItem.SeatsCount == 0)
            {
                return null;
            }
            var createdIem =  _ticketRepository.Create(item);

            await _ticketRepository.UnitOfWork.SaveChangesAsync();
            await _performanceRepository.UnitOfWork.SaveChangesAsync();

            return createdIem;
        }

        public async Task<Ticket> DeleteAsync(int ticketId)
        {


             var ticket = await _ticketRepository.GetById(ticketId);
          

            if (ticket == null)
            {
                return null;
            }
            // ticket.PPerformanceDate= new Performance();
   /*         ticket.PPerformanceId.Date = ticket.PerfDate;
            ticket.PPerformanceId.Name = ticket.PerfDate;
            ticket.PPerformanceId.Date = ticket.PerfDate;
            ticket.PPerformanceId.Date = ticket.PerfDate;*/

          /*  ticket.PPerformanceName = new Performance();
            ticket.PPerformancePrice = new Performance();*/

            _ticketRepository.Delete(ticket);
await _performanceRepository.UnitOfWork.SaveChangesAsync();
            await _ticketRepository.UnitOfWork.SaveChangesAsync();
            

            return ticket;
        }

        public async Task<Ticket> UpdateAsync(Ticket item)
        {
            var ticket = await _ticketRepository.GetByIdAsync(item.Id);
            if (ticket == null)
            {
                return null;
            }

            ticket.PerfName = item.PerfName;
            ticket.IsConfirmed = item.IsConfirmed;
            ticket.PerfDate = item.PerfDate;
            ticket.PerfId = item.PerfId;
            ticket.PersonName = item.PersonName;
            ticket.Phone = item.Phone;
            ticket.PerfPrice = item.PerfPrice;
            ticket.Email = item.Email;
            ticket.CreditCard = item.CreditCard;

            _ticketRepository.Update(ticket);

            await _ticketRepository.UnitOfWork.SaveChangesAsync();

            return ticket;
        }
    }
}

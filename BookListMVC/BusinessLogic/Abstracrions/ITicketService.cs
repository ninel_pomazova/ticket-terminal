﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.BusinessLogic.Abstracrions
{
    public interface ITicketService
    {
        Task<Ticket> DeleteAsync(int ticketId);
        Task<Ticket> UpdateAsync(Ticket item);
        Task<Ticket> CreateAsync(Ticket item);
    }
}

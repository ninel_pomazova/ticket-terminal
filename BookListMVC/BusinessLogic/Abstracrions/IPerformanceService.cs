﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.BusinessLogic.Abstracrions
{
    public interface IPerformanceService
    {
        Task<Performance> DeleteAsync(int perfId);
        Task<Performance> UpdateAsync(Performance item);
        Task<Performance> CreateAsync(Performance item);
    }
}

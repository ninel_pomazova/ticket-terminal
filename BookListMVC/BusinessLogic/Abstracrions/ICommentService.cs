﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tickets.Models;

namespace Tickets.BusinessLogic.Abstracrions
{
    public interface ICommentService
    {
        Task<Comment> DeleteAsync(int commentId);
        Task<Comment> UpdateAsync(Comment item);
        Task<Comment> CreateAsync(Comment item);
    }
}

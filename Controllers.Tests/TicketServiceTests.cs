﻿using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Services;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;
using Xunit;

namespace BusinessLogic.Tests
{
    public class TicketServiceTests
    {
        [Theory]
        [InlineData(1, "name", "email", "1234567", "123456789012345", 2, "perfname", "10-10-2020", 200, true)]
        public async Task CreateAsync_AllTicketsAlreadySold_ReturnNull(int id, string personName, string email, string phone, string creditCard, int perfId, string perfName, DateTime perfDate, decimal perfPrice, bool isConfirmed)
        {
            //arrange
            var ticket = new Ticket
            {
                Id = id,
                CreditCard = creditCard,
                Email = email,
                IsConfirmed = isConfirmed,
                PerfDate = perfDate,
                PerfId = perfId,
                PerfName = perfName,
                PerfPrice = perfPrice,
                PersonName = personName,
                Phone = phone,
          /*      PPerformanceDate = new Performance(),
                PPerformanceId = new Performance(),*/
                PPerformanceName = new Performance(),
              //  PPerformancePrice = new Performance()

            };

            var performance = new Performance
            {
                Id = ticket.PerfId,
                Date = ticket.PerfDate,
                Info = "some Info",
                Name = ticket.PerfName,
                TPerfDates = new Collection<Ticket>(),
                Price = ticket.PerfPrice,
                SeatsCount = 0,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };


            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();


            repositoryMock.Setup(x => x.GetByIdAsync(ticket.Id))
              .Returns(Task.FromResult(ticket));

            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
              .Returns(Task.FromResult(performance));

            repositoryMock.Setup(x => x.Create(ticket))
                .Returns(ticket);
            //.Returns(Task.FromResult(ticket));

            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //act
            var result = await service.CreateAsync(ticket);

            // assert
            Assert.Null(result);

        }

        [Theory]
        [InlineData(1, "name", "email", "1234567", "123456789012345", 2, "perfname", "10-10-2020" , 200, true)]
        public async Task CreateAsync_TicketsIsCreated_ReturnNewTicket(int id, string personName, string email, string phone, string creditCard, int perfId, string perfName, DateTime perfDate, decimal perfPrice, bool isConfirmed)
        {
            //arrange
            var ticket = new Ticket
            {
                Id = id,
                CreditCard = creditCard,
                Email = email,
                IsConfirmed = isConfirmed,
                PerfDate = perfDate,
                PerfId = perfId,
                PerfName = perfName,
                PerfPrice = perfPrice,
                PersonName =  personName,
                Phone = phone,
              /*  PPerformanceDate = new Performance(),
                PPerformanceId = new Performance(),*/
                PPerformanceName = new Performance(),
               // PPerformancePrice = new Performance()

            };
            var performance = new Performance
            {
                Id = ticket.PerfId,
                Date = ticket.PerfDate,
                Info = "some Info",
                Name = ticket.PerfName,
                TPerfDates = new Collection<Ticket>(),
                Price = ticket.PerfPrice,
                SeatsCount = 10,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };

            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();


            repositoryMock.Setup(x => x.GetByIdAsync(ticket.Id))
              .Returns(Task.FromResult(ticket));

            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
             .Returns(Task.FromResult(performance));

            repositoryMock.Setup(x => x.Create(ticket))
                .Returns(ticket);
              //.Returns(Task.FromResult(ticket));
            
            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //act
            var result = await service.CreateAsync(ticket);

            // assert
            Assert.NotNull(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_TicketsNotFound_ReturnNull(int ticketId)
        {
            //arrange
            var ticket = new Ticket
            {
                Id = ticketId,
                CreditCard = "card",
                Email = "email",
                IsConfirmed = It.IsAny<bool>(),
                PerfDate = It.IsAny<DateTime>(),
                PerfId = It.IsAny<int>(),
                PerfName = "perfName",
                PerfPrice = It.IsAny<decimal>(),
                PersonName = "pers Name",
                Phone = "phone"
            };

            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();


            repositoryMock.Setup(x => x.GetById(ticket.Id))
              .Returns(Task.FromResult<Ticket>(null));

            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMock.Setup(x => x.Delete(ticket));


            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //act
            var result = await service.DeleteAsync(ticketId);

            // assert
            Assert.Null(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_TicketsIsFound_ReturnTicketItem(int ticketId)
        {
            var ticket = new Ticket
            {
                Id = ticketId,
                CreditCard = "card",
                Email = "email",
                IsConfirmed = It.IsAny<bool>(),
                PerfDate = It.IsAny<DateTime>(),
                PerfId = It.IsAny<int>(),
                PerfName = "perfName",
                PerfPrice = It.IsAny<decimal>(),
                PersonName = "pers Name",
                Phone = "phone"
            };

            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();

            repositoryMock.Setup(x => x.GetById(ticket.Id))
              .Returns(Task.FromResult(ticket));
            repositoryMock.Setup(x => x.Delete(ticket));

            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //отримуємо результат
            var result = await service.DeleteAsync(ticketId);

            // перевірка що результат в нас позитивний, а саме not null
            Assert.NotNull(result);

        }

        [Fact]
        public async Task UpdateAsync_TicketsNotFound_ReturnNull()
        {
            //arrange
            var ticket = new Ticket
            {
                Id = 1,
                CreditCard = "card",
                Email = "email",
                IsConfirmed = It.IsAny<bool>(),
                PerfDate = It.IsAny<DateTime>(),
                PerfId = It.IsAny<int>(),
                PerfName = "perfName",
                PerfPrice = It.IsAny<decimal>(),
                PersonName = "pers Name",
                Phone = "phone"
            };

            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();

            repositoryMock.Setup(x => x.GetByIdAsync(ticket.Id))
              .Returns(Task.FromResult<Ticket>(null));
            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //act
            var result = await service.UpdateAsync(ticket);

            // assert
            Assert.Null(result);

        }

        [Fact]
        public async Task UpdateAsync_TicketsIsFound_ReturnTicket()
        {
            //arrange
            var ticket = new Ticket
            {
                Id = 1,
                CreditCard = "card",
                Email = "email",
                IsConfirmed = It.IsAny<bool>(),
                PerfDate = It.IsAny<DateTime>(),
                PerfId = It.IsAny<int>(),
                PerfName = "perfName",
                PerfPrice = It.IsAny<decimal>(),
                PersonName = "pers Name",
                Phone = "phone"
            };

            var repositoryMock = new Mock<ITicketRepository>();
            var repositoryMockPerf = new Mock<IPerformanceRepository>();

            repositoryMock.Setup(x => x.GetByIdAsync(ticket.Id))
              .Returns(Task.FromResult(ticket));
            repositoryMock.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new TicketService(repositoryMock.Object, repositoryMockPerf.Object);

            //act
            var result = await service.UpdateAsync(ticket);

            // assert
            Assert.NotNull(result);

        }


    }
}

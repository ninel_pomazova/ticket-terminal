﻿using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Services;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;
using Xunit;

namespace BusinessLogic.Tests
{
    public class CommentServiceTests
    {
        [Theory]
        [InlineData(1, "name","text")]
        public async Task CreateAsync_CommentIsCreated_ReturnNewTicket(int id, string personName, string text)
        {
            var comment = new Comment
            {
                Id = id,
                PersonName = personName,
                Text = text
            };

            var repositoryMockComment = new Mock<ICommentRepository>();
           
            repositoryMockComment.Setup(x => x.GetByIdAsync(comment.Id))
              .Returns(Task.FromResult(comment));
            repositoryMockComment.Setup(x => x.Create(comment)).Returns(await Task.FromResult(comment));
            repositoryMockComment.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new CommentService(repositoryMockComment.Object);

            //act
            var result = await service.CreateAsync(comment);

            // assert
            Assert.NotNull(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_TicketsNotFound_ReturnNull(int ticketId)
        {
            var comment = new Comment
            {
                Id = ticketId,
                PersonName = "name",
                Text = "text"
            };

            var repositoryMockComment = new Mock<ICommentRepository>();
            repositoryMockComment.Setup(x => x.Delete(comment));
            repositoryMockComment.Setup(x => x.GetByIdAsync(comment.Id))
              .Returns(Task.FromResult<Comment>(null));
            repositoryMockComment.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new CommentService(repositoryMockComment.Object);


            //act
            var result = await service.DeleteAsync(ticketId);

            // assert
            Assert.Null(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_CommentIsFound_ReturnTicketItem(int ticketId)
        {
            var comment = new Comment
            {
                Id = ticketId,
                PersonName = "name",
                Text = "text"
            };

            var repositoryMockComment = new Mock<ICommentRepository>();

            repositoryMockComment.Setup(x => x.GetByIdAsync(comment.Id))
              .Returns(Task.FromResult(comment));
            repositoryMockComment.Setup(x => x.Delete(comment));
            repositoryMockComment.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new CommentService(repositoryMockComment.Object);


            //отримуємо результат
            var result = await service.DeleteAsync(comment.Id);

            // перевірка що результат в нас позитивний, а саме not null
            Assert.NotNull(result);

        }

        [Fact]
        public async Task UpdateAsync_CommentNotFound_ReturnNull()
        {
            var comment = new Comment
            {
                Id = 1,
                PersonName = "name",
                Text = "text"
            };

            var repositoryMockComment = new Mock<ICommentRepository>();
            repositoryMockComment.Setup(x => x.Update(comment));
            repositoryMockComment.Setup(x => x.GetByIdAsync(comment.Id))
              .Returns(Task.FromResult<Comment>(null));
            repositoryMockComment.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new CommentService(repositoryMockComment.Object);

            //act
            var result = await service.UpdateAsync(comment);
            // assert
            Assert.Null(result);

        }

        [Fact]
        public async Task UpdateAsync_CommentIsFound_ReturnTicket()
        {
            //arrange
            var comment = new Comment
            {
                Id = 1,
                PersonName = "name",
                Text = "text"
            
            
            };

            var repositoryMockComment = new Mock<ICommentRepository>();

            repositoryMockComment.Setup(x => x.GetByIdAsync(comment.Id))
              .Returns(Task.FromResult(comment));
            repositoryMockComment.Setup(x => x.Update(comment));
            repositoryMockComment.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new CommentService(repositoryMockComment.Object);

            //act
            var result = await service.UpdateAsync(comment);

            // assert
            Assert.NotNull(result);

        }


    }
}

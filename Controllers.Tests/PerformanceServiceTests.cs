﻿using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Tickets.BusinessLogic.Abstracrions;
using Tickets.BusinessLogic.Services;
using Tickets.Infrastructure.Abstractions;
using Tickets.Models;
using Xunit;

namespace BusinessLogic.Tests
{
    public class PerformanceServiceTests
    {

        [Theory]
        [InlineData(1, "name", "info",200, "10-10-2020", "venue", 3)]
        public async Task CreateAsync_PerformanceCreates_ReturnPerformance(int id, string perfName, string info, decimal perfPrice, DateTime perfDate,  string  venue, int seats)
        {
            var performance = new Performance
            {
                Id = id,
                Date = perfDate,
                Info = info,
                Name = perfName,
                Price = perfPrice,
                SeatsCount = seats,
                Venue = venue,
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfDates = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };


            var repositoryMockPerf = new Mock<IPerformanceRepository>();


                 repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
              .Returns(Task.FromResult(performance));

            repositoryMockPerf.Setup(x => x.Create(performance))
                .Returns(performance);
            //.Returns(Task.FromResult(ticket));

            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new PerformanceService(repositoryMockPerf.Object);

            //act
            var result = await service.CreateAsync(performance);

            // assert
            Assert.NotNull(result);

        }

        [Fact]
        public async Task UpdateAsync_PerformancesNotFound_ReturnNull()
        {
            //arrange
            var performance = new Performance
            {
                Id = 1,
                Date = It.IsAny<DateTime>(),
                Info = "some Info",
                Name = "name",
                Price = It.IsAny<decimal>(),
                SeatsCount = 10,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(), 
                TPerfDates = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };

            var repositoryMockPerf = new Mock<IPerformanceRepository>();
            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
              .Returns(Task.FromResult<Performance>(null));

            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new PerformanceService(repositoryMockPerf.Object);

            //act
            var result = await service.UpdateAsync(performance);

            // assert
            Assert.Null(result);

        }

        [Fact]
        public async Task UpdateAsync_PerformanceIsFound_ReturnTicket()
        {
            var performance = new Performance
            {
                Id = 1,
                Date = It.IsAny<DateTime>(),
                Info = "some Info",
                Name = "name",
                Price = It.IsAny<decimal>(),
                SeatsCount = 10,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfDates = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };

            var repositoryMockPerf = new Mock<IPerformanceRepository>();
            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
              .Returns(Task.FromResult(performance));

            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));

            // створюємо сервіс і передаємо в нього репозиторій
            var service = new PerformanceService(repositoryMockPerf.Object);

            //act
            var result = await service.UpdateAsync(performance);

            // assert
            Assert.NotNull(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_PerformanceNotFound_ReturnNull(int ticketId)
        {
            //arrange
            var performance = new Performance
            {
                Id = 1,
                Date = It.IsAny<DateTime>(),
                Info = "some Info",
                Name = "name",
                Price = It.IsAny<decimal>(),
                SeatsCount = 10,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfDates = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };

            var repositoryMockPerf = new Mock<IPerformanceRepository>();
            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
                .Returns(Task.FromResult<Performance>(null));



            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.Delete(performance));


            // створюємо сервіс і передаємо в нього репозиторій
            var service = new PerformanceService(repositoryMockPerf.Object);

            //act
            var result = await service.DeleteAsync(ticketId);

            // assert
            Assert.Null(result);

        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteAsync_PerformanceIsFound_ReturnTicketItem(int ticketId)
        {
            //arrange
            var performance = new Performance
            {
                Id = 1,
                Date = It.IsAny<DateTime>(),
                Info = "some Info",
                Name = "name",
                Price = It.IsAny<decimal>(),
                SeatsCount = 10,
                Venue = "Venue",
                TPerfIds = new Collection<Ticket>(),
                TPerfNames = new Collection<Ticket>(),
                TPerfDates = new Collection<Ticket>(),
                TPerfPrices = new Collection<Ticket>()
            };

            var repositoryMockPerf = new Mock<IPerformanceRepository>();
            repositoryMockPerf.Setup(x => x.GetByIdAsync(performance.Id))
                .Returns(Task.FromResult(performance));



            repositoryMockPerf.Setup(x => x.UnitOfWork.SaveChangesAsync(default));
            repositoryMockPerf.Setup(x => x.Delete(performance));


            // створюємо сервіс і передаємо в нього репозиторій
            var service = new PerformanceService(repositoryMockPerf.Object);

            //act
            var result = await service.DeleteAsync(ticketId);

            // assert
            Assert.NotNull(result);

        }

    }
}
